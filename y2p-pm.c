#include <stdint.h>
#include <sys/io.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ec_access.h>
#include <lenovo_features.h>

extern lenovo_features_t lnv_f[3];

static void show_info(void)
{
    for (int i = 0; i < N_FEATURES; i++) 
        printf("[INFO]\t %s: %d\n", lnv_f[i].info, lnv_f[i].get_state());
}

static void show_help(void)
{
    printf("[INFO]\tTo enable/disable lenovo yoga 2 pro power management features,\n");
    printf("[INFO]\tpass feature key and its new status to the program\n");
    printf("[INFO]\tProgram syntax: y2pfeature -FEATURE status");
    printf("[INFO]\tFeatures: \n");
    printf("[INFO]\t\t[-b on/off] enable/disable battery charge limit\n");
    printf("[INFO]\t\t[-u on/off] enable/disable usb charging\n");
    printf("[INFO]\t\t[-q on/off] enable/disable quick battery charge\n");
    printf("[INFO]\tTo dump the whole EC area, pass [--dump] argument\n");
    printf("[INFO]\tExamples: \n");
    printf("[INFO]\t\ty2pfeature -b on -u on\n");
    printf("[INFO]\t\t--enables battery charge limit and usb charging\n");
    printf("[INFO]\t\ty2pfeature -b off\n");
    printf("[INFO]\t\t--disables battery charge limit\n");
    printf("[INFO]\t\ty2pfeature --dump\n");
    printf("[INFO]\t\t--dumps EC area\n");
    printf("[INFO]\t\ty2pfeature -b\n");
    printf("[INFO]\t\t--whows current battery charge limit status\n");
}

int main(int argc, char *argv[])
{  
    if (init() != 0) {
        printf("[ERROR]\t Failed to set permissions to access the port!\n");
        exit(1);
    }

    if (argc < 2) {
        show_info();
    }
    else {
        int i = 1;
        int8_t paramst = -1;
        for (; i < argc; ++i) {
            int feature_id = -1;
            //printf("argv[%d] = '%s'\n", i, argv[i]);
            if (strcmp(argv[i], "-b") == 0) {
                feature_id = 0;
            } else if (strcmp(argv[i], "-u") == 0) {
                feature_id = 1;
            } else if (strcmp(argv[i], "-q") == 0) {
                feature_id = 2;
            } else if (strcmp(argv[i],"--dump") == 0) {
                dump_all_regs();
            } else if (strcmp(argv[i], "-h") == 0) {
                show_help();
                return 0;
            } else {
                printf("[ERROR]\t Unknown parameter!\n");
                return 0;
            }
            if (i + 1 < argc) {
                if (strcmp(argv[i+1], "on") == 0) {
                    paramst = 1;
                    i++;
                }
                else if (strcmp(argv[i+1], "off") == 0) {
                    paramst = 0;
                    i++;
                }
            }
            if (feature_id != -1) {
                if (paramst == -1)
                    printf("[INFO]\t Current %s state is %d\n", lnv_f[feature_id].info, lnv_f[feature_id].get_state());
                else if (paramst == lnv_f[feature_id].get_state())
                    printf("[INFO]\t %s is already set to %d. Nothing to do.\n", lnv_f[feature_id].info, lnv_f[feature_id].get_state());
                else {
                    lnv_f[feature_id].set_state(paramst);
                    printf("[INFO]\t %s is set to %d\n", lnv_f[feature_id].info, lnv_f[feature_id].get_state());
                }
            }
        }
    }
    return 0;
}
