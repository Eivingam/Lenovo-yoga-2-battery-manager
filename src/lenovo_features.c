#include <stdint.h>
#include <sys/io.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ec_access.h>
#include <lenovo_features.h>

void dump_all_regs(void)
{
    uint8_t val;
    int i;
    printf("[INFO]\t EC reg dump:");

    for (i = 0x00; i <= 0xff; i++)
    {
        if ((i % 16) == 0)
        {
            printf("\n 0x%02x: ", i);
        }

        val = read_ec(i);
        printf("%02x ", val);
    }

    printf("\n");
}

int get_charge_threshold_status(void)
{
    uint8_t tmp1, tmp2;
    tmp1 = read_ec(R_EC_BATT_PORT);
    tmp2 = read_ec(R_ED_BATT_PORT);
    if (tmp1 == BATT_LIMIT_OFF_EC && (tmp2 == BATT_LIMIT_OFF_ED || tmp2 == BATT_LIMIT_OFF_DCH_ED)) {
        return 0;
    } else if ((tmp1 == BATT_LIMIT_ON_EC || tmp1 == BATT_LIMIT_ON_AND_QC_EC) && tmp2 == BATT_LIMIT_ON_ED) {
        return 1;
    } else {
        return -1;
    }
}

void set_charge_threshold(uint8_t value)
{   
    if (value == 0) {
        //enable full charge
        write_ec(R_EC_BATT_PORT, BATT_LIMIT_OFF_EC);
        write_ec(R_ED_BATT_PORT, BATT_LIMIT_OFF_ED);
    } else if (value == 1) {
        //limit battery charge
        write_ec(R_EC_BATT_PORT, BATT_LIMIT_ON_EC);
        write_ec(R_ED_BATT_PORT, BATT_LIMIT_ON_ED);
    }
}

int get_usb_charge_status(void)
{
    uint8_t tmp1;
    tmp1 = read_ec(R_43_USB_CHARGE);
    if (tmp1 == USB_CHARGE_OFF) {
        return 0;
    } else if (tmp1 == USB_CHARGE_ON) {
        return 1;
    } else {
        return -1;
    }
}

void set_usb_charge_status(uint8_t value)
{
    if (value == 0)
        write_ec(R_43_USB_CHARGE, USB_CHARGE_OFF);
    else if (value == 1)
        write_ec(R_43_USB_CHARGE, USB_CHARGE_ON);
    return;
}

int get_quick_charge_status(void)
{
    uint8_t tmp1;
    tmp1 = read_ec(R_E2_QC_PORT);
    if (tmp1 == QC_OFF) {
        return 0;
    } else if (tmp1 == QC_ON) {
        return 1;
    } else {
        return -1;
    }
}

void set_quick_charge_status(uint8_t value)
{
    if (value == 0)
        write_ec(R_E2_QC_PORT, QC_OFF);
    else if (value == 1)
        write_ec(R_E2_QC_PORT, QC_ON);
    return;
}

lenovo_features_t lnv_f[3] =   {{ .set_state = set_charge_threshold, .get_state = get_charge_threshold_status, .info = "battery charge threshold"},
                    {  .set_state = set_usb_charge_status, .get_state = get_usb_charge_status, .info = "USB charging"},
                    {  .set_state = set_quick_charge_status, .get_state = get_quick_charge_status, .info = "Quick charge"}};
