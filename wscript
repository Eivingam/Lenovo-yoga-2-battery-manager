top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')
 	
def configure(conf):
    conf.load('compiler_c')

def build(bld):
    bld.program(source = bld.path.ant_glob('**/*.c'), includes='include', target = 'y2p-pm', features = 'c cprogram')
